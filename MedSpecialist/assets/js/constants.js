var APP_CONSTANTS = {
    BASE_URL: "http://fajjemobile.info/MedSpecialist/",
	REGISTER: "register.php",
	MSG_ACTION_FAILED: "Failed",
	LOGIN: "login.php"
}

var JSON_CONSTANTS = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
    CONTENT_TYPE: "text/json",
    DATA_TYPE: "json",
    ACCEPT: "text/json",
    STATUS_CODE_404: "404",
    STATUS_CODE_401: "401",
    STATUS_CODE_403: "403",
    STATUS_CODE_302: "302"
}
